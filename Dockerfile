FROM python:3.8

ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip 

RUN mkdir /app
WORKDIR /app
COPY ./src /app

# install dependencies
RUN apt-get update && apt-get -y install netcat

RUN pip install -r ./requirements.txt