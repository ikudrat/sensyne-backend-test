rundocker:
	@docker-compose up --build

tests:
	@docker-compose run --rm app sh -c "python manage.py test"