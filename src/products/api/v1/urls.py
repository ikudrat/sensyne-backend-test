from django.urls import path
from .views import ProductListAPIView, ProductCreateAPIView, ProductDetailAPIView

urlpatterns = [
    path('products', ProductListAPIView.as_view(), name="product_list"),
    path('product', ProductCreateAPIView.as_view(), name="product_create"),
    path('product/<int:pk>', ProductDetailAPIView.as_view(), name="product_detail")
]