from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status

class ProductTestCase(APITestCase):

    def test_create_product(self):
        """ testing product is created successfully"""

        data = {"name": "TestName", "price": 9.93}
        response = self.client.post(reverse("product_create"), data)        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["name"], "TestName")
        
    def test_list_product(self):
        """ testing products list endpoint"""

        count = 5
        data = {"name": "TestName", "price": 9.93}
        
        for _ in range(count):
            self.client.post(reverse("product_create"), data)

        response = self.client.get(reverse("product_list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), count)
